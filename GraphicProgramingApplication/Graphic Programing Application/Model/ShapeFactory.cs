﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphic_Programing_Application.Model
{
    public class ShapeFactory
    {
        public Shape GetShape(string shapeType)
        {
            shapeType = shapeType.ToUpper().Trim();

            if (shapeType.Equals("RECTANGLE"))
            {
                return new Rectangle();
            }
            if (shapeType.Equals("SQAURE"))
            {
                return new Square();
            }
            if (shapeType.Equals("CIRCLE"))
            {
                return new Circle();
            }
            if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle();
            }
            else
            {
                System.ArgumentException argE = new ArgumentException($"Shape Factory error: {shapeType} is not a valid shape");
                throw argE;
            }

        }

        // TODO Add Constructor to take shape parameters.

    }
}
