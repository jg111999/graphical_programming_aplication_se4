﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphic_Programing_Application.Model
{
    class Triangle : Shape
    {
        int width { get; set; }
        int height { get; set; }
 

        public Triangle() : base()
        {
            width = height = 50;
            
        }

        public Triangle(Color colour, int x, int y, int width, int height):base(colour, x, y)
        {
            this.width = width;
            this.height = height;
        }

        public override void Set(Color colour, params int[] paramList)
        {
            base.Set(colour, paramList[0], paramList[1]);
            this.width = paramList[2];
            this.height = paramList[3];
        }

        public override void Draw(Graphics g)
        {
            Pen pen = new Pen(outLineColour, outLineWeight);
            SolidBrush brush = new SolidBrush(colour);
            g.FillPolygon(brush, GetTrianglePoints(xPos, yPos, width, height));
            g.DrawPolygon(pen, GetTrianglePoints(xPos, yPos, width, height));
        }

        public override void SetWidth(int width)
        {
            this.width = width;
        }

        public override void SetHeight(int height)
        {
            this.height = height;
        }

        //returns an array of points calculated from the x, y, width and height
        private Point[] GetTrianglePoints(int x, int y, int width, int height)
        {
            Point point1 = new Point((x + width / 2), y);
            Point point2 = new Point(x, (y + height));
            Point point3 = new Point((x + width), (y + height));
            Point[] points = { point1, point2, point3, };

            return points;
        }

        public override void SetSize(int size) 
        {
            this.width = size;
            this.height = size;
        }
        public override void SetRadius(int radius) { }
        
    }
}
