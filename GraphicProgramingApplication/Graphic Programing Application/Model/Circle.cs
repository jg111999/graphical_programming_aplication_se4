﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphic_Programing_Application.Model
{
    class Circle : Shape
    {
        int radius { get; set; }

        public Circle() : base()
        {
        }

        public Circle(Color colour, int x, int y, int radius): base(colour, x, y)
        {
            this.radius = radius;
        }

        public override void Set(Color colour, params int[] paramList)
        {
            base.Set(colour, paramList[0],paramList[1]);
            this.radius = paramList[2];
        }

        public override void SetRadius(int radius)
        {
            this.radius = radius;
        }

        public override void Draw(Graphics g)
        {
            Pen pen = new Pen(outLineColour, outLineWeight);
            SolidBrush brush = new SolidBrush(colour);
            g.FillEllipse(brush, xPos, yPos, radius * 2, radius * 2);
            g.DrawEllipse(pen, xPos, yPos, radius * 2, radius * 2);
        }

        public override void SetWidth(int width) { }
        public override void SetHeight(int height) { }
        public override void SetSize(int size) { }

        
    }
}
