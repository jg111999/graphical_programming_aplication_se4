﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphic_Programing_Application.Model
{
    public class Square : Rectangle
    {
        private int size { get; set; }

        public Square() : base()
        {

        }

        public Square(Color colour, int x, int y, int size): base(colour, x, y, size, size)
        {
            this.size = size;
            
        }

        public override void Set(Color colour, params int[] paramList)
        {
            base.Set(colour, paramList[0], paramList[1],paramList[2],paramList[2]);
            
        }

        public override void Draw(Graphics g)
        {
            base.Draw(g);
        }

     }
}
