﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphic_Programing_Application.Model.Interface
{
    interface IShape
    {
        void Set(Color color, params int[] paramListist);
        void SetColour(Color color);
        void SetOutlineColour(Color colour);
        void SetWidth(int width);
        void SetHeight(int height);
        void SetX(int x);
        void SetY(int y);
        void SetSize(int size);
        void SetRadius(int radius);
        void SetOutLineWeight(int weight);
        void Draw(Graphics g);

    }
}
