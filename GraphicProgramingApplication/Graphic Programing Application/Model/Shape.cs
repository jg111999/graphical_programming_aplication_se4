﻿using Graphic_Programing_Application.Model.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphic_Programing_Application.Model
{
    public abstract class Shape :IShape
    {
        protected int xPos { get; set; }
        protected int yPos { get; set; }
        protected Color colour { get; set; }
        protected Color outLineColour { get; set; }
        protected int outLineWeight { get; set; }

        //Constants
        protected int DEFAULT_POSITION = 10;
        protected Color DEFAULT_COLOUR = Color.Black;
        protected int DEFAULT_WEIGHT = 2;

        public Shape()
        {
            colour = DEFAULT_COLOUR;
            xPos = yPos = DEFAULT_POSITION;
            outLineColour = DEFAULT_COLOUR;
            outLineWeight = DEFAULT_WEIGHT;
        }

        public Shape(Color colour, int x, int y) 
        {
            this.xPos = x;
            this.yPos = y;
            this.colour = colour;
        }

        public virtual void Set(Color colour, params int[] paramList)
        {
            this.xPos = paramList[0];
            this.yPos = paramList[1];
            this.colour = colour;
        }

        public void SetColour(Color colour)
        {
            this.colour = colour;
        }

        public void SetOutlineColour(Color colour)
        {
            this.outLineColour = colour;
        }

        public void SetX(int x)
        {
            this.xPos = x;
        }

        public void SetY(int y)
        {
            this.yPos = y;
        }
        public void SetOutLineWeight(int weight)
        {
            this.outLineWeight = weight;
        }

        public abstract void SetWidth(int width);
        public abstract void SetHeight(int height);
        public abstract void Draw(Graphics g);
        public abstract void SetSize(int size);
        public abstract void SetRadius(int radius);

        
    }
}
