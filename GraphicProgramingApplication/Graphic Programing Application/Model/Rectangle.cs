﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphic_Programing_Application.Model
{
    public class Rectangle : Shape
    {
        int width { get; set; }
        int height { get; set; }

        public Rectangle() : base()
        {
            width = height = 50;
        }

        public Rectangle(Color colour, int x, int y, int width, int height): base(colour, x, y)
        {
            this.width = width;
            this.height = height;
        }

        public override void Set(Color colour, params int[] paramList)
        {
            base.Set(colour, paramList[0], paramList[1]);
            this.width = paramList[2];
            this.height = paramList[3];
        }

        public override void Draw(Graphics g)
        {
            Pen pen = new Pen(outLineColour, outLineWeight);
            SolidBrush brush = new SolidBrush(colour);
            g.FillRectangle(brush, xPos, yPos, width, height);
            g.DrawRectangle(pen, xPos, yPos, width, height);
        }

        public override void SetWidth(int width)
        {
            this.width = width;
        }

        public override void SetHeight(int height)
        {
            this.height = height;
        }

        public override void SetSize(int size) 
        {
            this.width = size;
            this.height = size;
        }
        public override void SetRadius(int radius) { }
     }
}
