﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Graphic_Programing_Application.Model;



namespace Graphic_Programing_Application
{
    public partial class GraphicProgrammingGUI : Form
    {
        private string WELCOME_TEXT = ""+
            "         .---------------------------------.\n"+
            "         |  .---------------------------.  |\n"+
            "         |[]|          Welcome          |[]|\n"+
            "         |  |            to             |  |\n"+
            "         |  |     Grapic Programmer     |  |\n"+
            "         |  |                           |  |\n"+
            "         |  |                           |  |\n"+
            "         |  |         Developed         |  |\n"+
            "         |  |            by             |  |\n"+
            "         |  |        John Gilson        |  |\n"+
            "         |  |          For SE4          |  |\n"+
            "         |  `---------------------------'  |\n"+
            "         |      __________________ _____   |\n"+
            "         |     |   ___            |     |  |\n"+
            "         |     |  |   |           |     |  |\n"+
            "         |     |  |   |           |     |  |\n"+
            "         |     |  |   |           |     |  |\n"+
            "         |     |  |___|           |     |  |\n"+
            "         \\_____|__________________|_____|__|";


        public GraphicProgrammingGUI()
        {   
            InitializeComponent();
           // ShowWelcomeMessage(5000);
            
        }

        private void ShowWelcomeMessage(int time)
        {
            richTextBox1.Text = WELCOME_TEXT;
            var timer = new Timer();
            timer.Interval = time;
            timer.Start();
            timer.Tick += delegate
            {
                richTextBox1.Text = string.Empty;
                timer.Stop();
                timer.Dispose();
            };
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            testShapes();
        }


        // Test method for testing if the shapes are displayed correctly
        //Deleate before release.
        private void testShapes()
        {
            ShapeFactory shapeFactory = new ShapeFactory();
            Graphics g = panel3.CreateGraphics();
            Shape s;
            s = shapeFactory.GetShape("Triangle");
            s.Set(Color.GreenYellow, 50, 30, 200, 100);
            s.Draw(g);
            s = shapeFactory.GetShape("sqaure");
            s.Set(Color.Beige, 200, 60, 100);
            s.Draw(g);
            s = shapeFactory.GetShape("circle");
            s.Set(Color.Red, 50, 200, 200, 100);
            s.Draw(g);
            s.SetColour(Color.White);
            s.SetRadius(100);
            s.SetX(45);
            s.SetY(50);
            s.SetOutlineColour(Color.Orange);
            s.SetOutLineWeight(10);
            s.Draw(g);
        }
    }   
}
